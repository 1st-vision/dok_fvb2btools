.. |label| replace:: 1st Vision B2B-Tools
.. |snippet| replace:: FvB2BTools
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.10
.. |version| replace:: 1.9.4
.. |php| replace:: 7.4


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Dieses Plugin ermöglicht das Deaktivieren von verschiedenen Kunden-Funktionen wie Preisdarstellung, Kaufmöglichkeit, Adressepflege für bestimmte Kundengruppen sowie weitere Einstellungen bzgl. der
Kundengruppen im Shop.

Frontend
--------



Backend
-------
.. image:: FvB2BTools1.png

:Kundengruppen ohne Kauffunktion: Kundengruppen, die hier angegeben werden, können im Shop keine Produkte in den Warenkorb legen und auch nicht zum Checkout gelangen.
:Preise für diese Gruppen ausblenden: Wenn diese Funktion aktiv ist, wird „Kundengruppen ohne Kaufoption“ das Recht entzogen, Artikel-Preise zu sehen. Es werden alle Preise im gesamten Shop ausgeblendet.
:Kundengruppen ohne Änderung der Rechnungsadresse: Kundengruppen, die hier angegeben werden, wird es verwehrt, die eigene Rechnungsadresse zu ändern. Die aus der Office Line importierte Adresse ist fest als Rechnungsadresse verankert und kann nicht geändert werden. 
:Kundengruppen ohne Änderung der Lieferadresse: Kundengruppen, die hier angegeben werden (und keine Adressen editieren dürfen), wird es verwehrt, die eigene Lieferadresse zu ändern. Die aus der Office Line importierte Adresse ist fest als Lieferadresse verankert und kann nicht geändert werden.
:UStID der Lieferadresse in Bestellung übertragen: Wenn dies Funktion aktiviert wird, wird die UStID der Lieferadresse in das FreitextFeld "fv_vat_id" bei der Bestellung abgelegt.
:"UStID vergessen?" einblenden bei Auswahl "Firma" in Lieferadresse: Wenn diese Funktion aktiviert ist, wird eine Meldung in der Kasse eingeblendet wenn die Lieferadresse eine Firma ist.
:"UStID vergessen?" einblenden bei Angabe einer Rechnungs-UStID: Wenn diese Funktion aktiviert ist, wird eine Meldung in der Kasse eingeblendet wenn eine Rechnungs-UStID hinterlegt ist.
:Shop für Gäste sperren / Loginseite vorschalten: Wenn diese Funktion aktiviert ist, wird die LogIn-Seite als Startseite eingeblendet.
:Inhaltsseiten, die von der Sperrung ausgeschlossen sind: Hier können die einzelnen Infoseiten von der Sperrung aufgehoben werden.
:Widget, die von der Sperrung ausgeschlossen: Hier können die Widget, wie zB. Einkaufswelten von der Sperrung aufgehoben werden.
:Kunden nach Registrierung deaktivieren: Wenn diese Funktion aktiviert ist, wird ein Kunde der sich neuregistriert auf deaktiviert gesetzt und es wird statt der "Mein-Konto"-Seite eine Einkaufswelt angezeigt.
:Zieleinkaufswelt nach Registrierung: Hier definieren Sie welche Einkaufswelt(Landing-Page) Sie nach der Registrierung einblenden wollen.
:Attribut für Freihausgrenze: Hier hinterlegen Sie das Freitext-Feld für die Freihausgrenze die beim Kunden hinterlegt ist.
:Mindestbestellwert: Hier hinterlegen Sie das Freitext-Feld für den Mindestbestellwert der beim Kunden hinterlegt ist.
:Zuschlag: Hier hinterlegen Sie das Freitext-Feld für den Zuschlag wenn der Mindestbestellwert der beim Kunden hinterlegt ist berechnet werden soll.
:Registrierung blockieren: Ja bedeutet das man sich nicht registrieren kann
:Zielinfoseite bei Registrierung: Wenn die Registrierung geblockt wird kann man hier eine ID einer Infoseite hinterlegen oder eine URL hinterlegen (https://hier-ihre-domain.de)
:Anzeige der Bestellmenge: Wenn "manuell" eingestellt ist, so muss die gewünschte Bestellmenge auf der Artikeldetailseite in einem Textfeld eingegeben werden, bei "dropdown" erscheint statt dem
    Textfeld ein Dropdownmenü
:Zuordnung zur Kundengruppe nach der Registrierung: Hier kann eine Kundengruppe ausgewählt werden, der ein neuer Benutzer nach erfolgter Neuregistrierung automatisch zugeordnet wird. Ist keine
    Kundengruppe ausgewählt, so wird ein Neukunde nachwievor der Standardgruppe zugeordnet
:Persönliche Daten in Rechnungsadresse übernehmen für diese Kundengruppen aktivieren: Hier kann eine odere mehrere Kundengruppe ausgewählt werden, diese Können dann daten auf der Persönlichen Seite hinterlegen und es werden diese daten automatisch
    bei der Rechnungsadresse hinterlegt.
    
Textbausteine
_____________

:FvVatIdEmptyWarning: Haben Sie vergessen, Ihre Umsatzsteuer-ID in der Lieferadresse anzugeben?


technische Beschreibung
------------------------
folgende Freitextfelder werden angelegt

:fv_vat_id: 's_order_shippingaddress_attributes', 'fv_vat_id', 'string'


Modifizierte Template-Dateien
-----------------------------
:/account/index.tpl:
:/address/ajax_selection.tpl:
:/address/form.tpl:
:/address/index.tpl:
:/checkout/confirm.tpl:
:/compare/col.tpl:
:/compare/col_description.tpl:
:/detail/block_price.tpl:
:/detail/buy.tpl:
:/detail/data.tpl:
:/listing/product-box/box-basic.tpl:
:/listing/product-box/box-minimal.tpl:
:/listing/product-box/button-buy.tpl:
:/listing/product-box/product-price.tpl:
:/listing/product-box/product-price-unit.tpl:
:/note/item.tpl:
:/register/index.tpl:
:/search/ajax.tpl:


